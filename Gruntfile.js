module.exports = function(grunt){
	grunt.initConfig({
		uglify: {
			assets:{
				src: 'assets/**/*.js',
				dest: 'build/js/assets.min.js'
			}
		},
		concat: {
			css:{
				src: 'assets/**/*.css',
				dest: 'build/css/styles.min.css'
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
}