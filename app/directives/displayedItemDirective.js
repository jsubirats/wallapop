angularApp.directive('displayedItem', function(Item){
    return{
        restrict: 'E',
        scope:{
            item: '='
        },
        templateUrl:'app/directives/displayedItemTemplate.html',
        controller: function($scope, $rootScope){
            $scope.markAsFavourite = function(item){
                item.favourite = !item.favourite;
                if(item.favourite == true){
                    $rootScope.favouriteItems.push(item);
                }
                else{
                    for(i = 0; i < $rootScope.favouriteItems.length; ++i){
                        if($rootScope.favouriteItems[i].id == item.id){
                            $rootScope.favouriteItems.splice(i,1);
                            break;
                        }
                    }
                }
                new Item(item).put();
            }
        }
    }
});