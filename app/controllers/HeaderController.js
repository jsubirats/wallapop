angularApp.controller('HeaderController', ['$scope', '$rootScope', 'ItemService', function($scope, $rootScope, ItemService){
    
    $rootScope.favouriteItems = [];
    $scope.favouriteSearch = "";
    
    $scope.setOrder = function(field){
        $rootScope.orderByField = field;
    };
}]);