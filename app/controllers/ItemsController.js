angularApp.controller('ItemsController', ['$scope', '$rootScope', '$compile', '$timeout', 'ItemService', function($scope, $rootScope, $compile, $timeout, ItemService){
    
    $scope.items = [];
    $scope.size = 5;
    $scope.searchKey = "";
    $scope.timeout;
    
    $scope.loadMoreItemsClickHandler = function(){
        $scope.moreItems = ItemService.getItems({
            "first" : $scope.items.length,
            "length" : $scope.size,
            "key" : $scope.searchKey
        });
        $scope.items = $scope.items.concat($scope.moreItems); 
    }
    
    $scope.searchKeyChanges = function(){
        if($scope.timeout){
            $timeout.cancel($scope.timeout);
            $scope.waitingResponse = false;
        }
        $scope.timeout = $timeout(function(){
            if($scope.searchKey == ""){
            $scope.items = [];
            }
            else{
                 $scope.items = ItemService.getItems({
                    "first" : 0,
                    "length" : $scope.size,
                    "key" : $scope.searchKey
                });
                $scope.timeout = null;
            }
        }, 300);
    }
}]);