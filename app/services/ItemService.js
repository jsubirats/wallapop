angularApp.service('ItemService', function($http, Item){
    this.getItems = function (params) {    
        
        /*
        This method simulates something like this:
        $http.get('api/items' + '?first=' + params.first + '&length=' + params.lenght);
        */
        
        var data = JSON.parse(localStorage.getItem("items"));
        var filteredItems = [];
        var paginatedItems = [];
        
        // Filter:
        if(params.key != undefined){
            for(i = 0; i < data.items.length; ++i)
            {
                if(data.items[i].title.toLowerCase().indexOf(params.key.toLowerCase()) > -1 ||
                   data.items[i].description.toLowerCase().indexOf(params.key.toLowerCase()) > -1 ||
                   String(data.items[i].price).indexOf(params.key) > -1 ||
                   data.items[i].email.toLowerCase().indexOf(params.key.toLowerCase()) > -1)
                    filteredItems.push(new Item(data.items[i]));
            }
        }
        
        // Paginate:
        if(params.first != undefined && params.length != undefined){
            for(i = params.first; i < params.length + params.first && i < filteredItems.length; ++i)
            {
                paginatedItems.push(new Item(filteredItems[i]));
            }
        }
        
        return paginatedItems;
    }
});