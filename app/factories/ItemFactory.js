angularApp.factory('Item', ['$http', function($http) {  
    function Item(itemData) {
        if (itemData) {
            this.setData(itemData);
        }
    };
    Item.prototype = {
        
        setData: function(itemData) {
            angular.extend(this, itemData);
        },
        post: function(){
            $http.post('api/items', this);
        },
        put: function() {
            /*
            This method simulates something like this:
            $http.put('api/items/' + this.id, this);
            */
            var data = JSON.parse(localStorage.getItem("items"));
            data.items[this.id - 1] = this;
            localStorage.setItem("items", JSON.stringify(data));
        },
        get: function(id){
            $http.get('api/items/' +id)
        }
    };
    return Item;
}]);