angularApp.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/views/itemsView.html',
        controller: 'ItemsController',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });